//
//  CeldaItem.swift
//  BusquedaEnML
//
//  Created by Emiliano Chiozzi on 27/10/2017.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class CeldaItem : UITableViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var descripcion: UILabel!
    
    @IBOutlet weak var precio: UILabel!
    
}
