//
//  ViewController.swift
//  BusquedaEnML
//
//  Created by Emiliano Chiozzi on 27/10/2017.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UISearchBarDelegate, UITableViewDelegate {

    @IBOutlet weak var tabla: UITableView!
    
    @IBOutlet weak var barraDeBusqueda: UISearchBar!
    
    var items = [Dictionary<String,Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tabla.dataSource = self
        self.tabla.delegate = self
        
        self.barraDeBusqueda.delegate = self
        
        self.navigationController?.navigationBar.barTintColor = UIColor.yellow
        
        self.title = "Mercado Libre"
        
        obtenerItems()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda : CeldaItem = CeldaItem()
        
        celda = self.tabla.dequeueReusableCell(withIdentifier: "CeldaItem") as!CeldaItem
        
        
        celda.descripcion.text = self.items[indexPath.row]["title"] as? String
        
        celda.precio.text = "$" + String(describing: self.items[indexPath.row]["price"]!)
        
        descargarImagenAsync(self.items[indexPath.row]["thumbnail"] as! String, celda)
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // hacer push a otra pantalla
        print("\(indexPath.row)")
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let detalle : UIViewController = storyboard.instantiateViewController(withIdentifier: "DetalleViewController")
        
        (detalle as! DetalleViewController).item = self.items[indexPath.row]
        
        self.navigationController?.pushViewController(detalle, animated: true)

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        obtenerItems()
        self.view.endEditing(true)
    }
    
    func obtenerItems() {
        
        let urlDeBusqueda:String = "https://api.mercadolibre.com/sites/MLA/search?q=" + self.barraDeBusqueda.text!.replacingOccurrences(of: " ", with: "%20")
        
        let url = URL(string: urlDeBusqueda)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if (data != nil) {
                do {
                    // Convertir Datos a JSON
                    let jsonSerialized = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String, Any>
                    
                    if (jsonSerialized!["results"] != nil) {
                        self.items = jsonSerialized!["results"] as! [Dictionary<String, Any>]
                        DispatchQueue.main.async {
                            self.tabla.reloadData()
                        }
                    }
                    
                }  catch let error as NSError {
                    print(error.localizedDescription)
                }
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    func descargarImagenAsync(_ image: String, _ cell: CeldaItem) {
        if (image != "") {
            let url = URL(string: image)
            let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if (data != nil) {
                    DispatchQueue.main.async {
                        cell.imagen.image = UIImage(data: data!)
                    }
                } else if let error = error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
        }
    }
    


}

