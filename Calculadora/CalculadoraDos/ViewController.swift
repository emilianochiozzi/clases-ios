﻿//
//  ViewController.swift
//  CalculadoraDos
//
//  Created by Emiliano Chiozzi on 10/14/17.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //ESTAMOS PROBANDO GIT DE NUEVO
    
    //ATRIBUTOS
    @IBOutlet weak var display: UILabel!
    var usuarioEstaEscribiendo : Bool = false
    var operador : String!
    var primerOperando : Int!
    var segundoOperador : Int!
    
    
    //METODOS
    @IBAction func clickEnDigito(_ sender: UIButton) {
        if (usuarioEstaEscribiendo) {
            display.text = display.text! + sender.currentTitle!
        } else {
            display.text = sender.currentTitle!
            usuarioEstaEscribiendo = true
        }
    }
    
    @IBAction func operadorSeleccionado(_ sender: UIButton) {
        operador = sender.currentTitle!
        primerOperando = Int(display.text!)
        usuarioEstaEscribiendo = false
    }
    
    @IBAction func igual(_ sender: UIButton) {
        segundoOperador = Int(display.text!)
        switch operador {
        case "+":
            display.text = String(primerOperando + segundoOperador)
        case "-":
            display.text = String(primerOperando - segundoOperador)
        case "*":
            display.text = String(primerOperando * segundoOperador)
        default:
            break
        }
    }
}
