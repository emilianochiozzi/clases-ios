//
//  ViewController.swift
//  ManejarPantallas
//
//  Created by Emiliano Chiozzi on 10/21/17.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class PrimeraPantalla : UIViewController {
    
    //Ciclo de Vida
    
    override func viewDidLoad() {
        print("viewDidLoad")
        self.title = "Primera Pantalla"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear")
        self.title = "Cambie Titulo"
    }
    
    
    @IBAction func presentar(_ sender: UIButton) {
        self.present(instanciarPantalla(), animated: true, completion: nil)
    }
    
    
    @IBAction func push(_ sender: UIButton) {
        self.navigationController?.pushViewController(instanciarPantalla(), animated: true)
    }
    
    func instanciarPantalla() -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil)
                            .instantiateViewController(withIdentifier: "SegundaPantalla")
    }
    
}

