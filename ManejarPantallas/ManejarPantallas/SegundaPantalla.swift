//
//  SegundaPantalla.swift
//  ManejarPantallas
//
//  Created by Emiliano Chiozzi on 10/21/17.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class SegundaPantalla : UIViewController {

    @IBAction func cerrar(_ sender: UIButton) {
        
        if (self.navigationController == nil) {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }


}
