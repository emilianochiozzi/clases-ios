//
//  CeldaPais.swift
//  ManejoDeTablas
//
//  Created by Emiliano Chiozzi on 10/21/17.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class CeldaPais : UITableViewCell {
    
    @IBOutlet weak var nombre: UILabel!
    
    @IBOutlet weak var bandera: UIImageView!
    
    
}
