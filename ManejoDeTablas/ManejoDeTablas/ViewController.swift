//
//  ViewController.swift
//  ManejoDeTablas
//
//  Created by Emiliano Chiozzi on 10/21/17.
//  Copyright © 2017 emiliano. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    

    var arrayDeDiccionarioPais : [Dictionary<String,String>] = [
        ["nombre": "Argenitna", "bandera": "ar"],
        ["nombre": "Brasil", "bandera": "br"],
        ["nombre": "Uruguay", "bandera": "uy"],
        ["nombre": "Paraguay", "bandera": "py"],
        ["nombre": "Bolivia", "bandera": "bo"],
        ["nombre": "Chile", "bandera": "cl"],
        ["nombre": "Colombia", "bandera": "co"],
        ["nombre": "Ecuador", "bandera": "ec"],
        ["nombre": "Venezuela", "bandera": "ve"],
        ["nombre": "Peru", "bandera": "pe"],
        ["nombre": "Italia", "bandera": "it"],
        ["nombre": "Francia", "bandera": "fr"],
        ["nombre": "Alemania", "bandera": "de"]
    ]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDeDiccionarioPais.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda : CeldaPais = CeldaPais()
        
        celda = self.tableView.dequeueReusableCell(withIdentifier: "CeldaPais") as! CeldaPais
        
//        print("\(indexPath.row)")
        
//        if (indexPath.row == 1) {
//            celda.contentView.backgroundColor = UIColor.red
//        } else {
//            celda.contentView.backgroundColor = UIColor.white
//        }
        
        celda.nombre.text = arrayDeDiccionarioPais[indexPath.row]["nombre"]
        
        celda.bandera.image = UIImage(named: arrayDeDiccionarioPais[indexPath.row]["bandera"]!)

        
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
    }



}

